package com.serai.api.core.order;

import java.io.Serializable;

    public class OrderDTO implements Serializable {
    private long id;
    private String name;
    private int quantity;
    private int price;

    public OrderDTO() {
        id = -1;
        name = null;
        quantity = 0;
        price = 0;
    }

    public OrderDTO(String name, int quantity, int price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public OrderDTO(int id, String name, int quantity, int price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public int getPrice() {
        return this.price;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}