package com.serai.api.core.order;

import com.serai.api.util.PageResult;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface OrderService {
  /**
   * Sample usage, see below.
   *
   * curl -X POST $HOST:$PORT/order \
   * -H "Content-Type: application/json" --data \
   * '{"name":"Meat Lover","quantity":1, "price":100}'
   *
   * @param body A JSON representation of the pizza order
   * @return A JSON representation of the newly created product
   */
  @PostMapping(value = "/order", consumes = "application/json", produces = "application/json")
  OrderDTO createOrder(@RequestBody(required = false) OrderDTO body);

  @GetMapping("/order")
  ResponseEntity<PageResult<OrderDTO>> find(@RequestParam(required = false) String name,
      @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size,
      @RequestParam(defaultValue = "name,desc") String[] sort);

  
  @GetMapping(value="/order/{orderId}", produces="application/json")    
  OrderDTO getOrder(@PathVariable long orderId,
      @RequestParam(value = "delay", required = false, defaultValue = "0") int delay,
      @RequestParam(value = "faultPercent", required = false, defaultValue = "0") int faultPercent);

  @DeleteMapping(value = "/order/{orderId}")
  ResponseEntity<HttpStatus> deleteOrder(@PathVariable long orderId);
}
