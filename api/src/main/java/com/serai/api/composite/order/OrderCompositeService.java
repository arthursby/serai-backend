package com.serai.api.composite.order;

import com.serai.api.core.order.OrderDTO;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.v3.oas.annotations.tags.Tag;
import reactor.core.publisher.Mono;
import io.swagger.v3.oas.annotations.Operation;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@SecurityRequirement(name = "security_auth")
@Tag(name = "OrderComposite", description = "REST API for composite order information.")
public interface OrderCompositeService {
        /**
         * Sample usage: "curl $HOST:$PORT/order-composite/1".
         *
         * @param orderId Id of the order
         * @return the composite product info, if found, else null
         */
        @Operation(summary = "${api.order-composite.get-composite-order.description}")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "${api.responseCodes.ok.description}"),
                        @ApiResponse(responseCode = "400", description = "${api.responseCodes.badRequest.description}"),
                        @ApiResponse(responseCode = "404", description = "${api.responseCodes.notFound.description}"),
                        @ApiResponse(responseCode = "422", description = "${api.responseCodes.unprocessableEntity.description}")
        })

        @PostMapping(value = "/order-composite", produces = "application/json")
        public Mono<ResponseEntity<OrderDTO>> createOrder(@RequestBody OrderDTO body);

        @PostMapping(value = "/order-composite/dispatch", produces = "application/json")
        public Mono<ResponseEntity<HttpStatus>> dispatchOrder(@RequestBody OrderDTO body);

        /**
         * Test secured api endpoint, only user SCOPE_SECURE has authority
         * 
         * @return HTTP.OK or ERROR
         */
        @GetMapping("/order-composite/secured")
        public ResponseEntity<HttpStatus> secured(@RequestParam String host);

        /**
         * Check API status.
         * 
         * @return "OK"
         */
        @GetMapping("/order-composite/status")
        public Mono<String> status();

        @GetMapping(value = "/order-composite/", produces = "application/json")
        public Mono<String> find(@RequestParam(required = false) String name,
                        @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size,
                        @RequestParam(defaultValue = "name,desc") String[] sort);

        @GetMapping(value = "/order-composite/{orderId}", produces = "application/json")
        public Mono<OrderDTO> getOrder(@RequestHeader HttpHeaders headers,
                        @PathVariable long orderId,
                        @RequestParam(value = "delay", required = false, defaultValue = "0") int delay,
                        @RequestParam(value = "faultPercent", required = false, defaultValue = "0") int faultPercent);

        /**
         * Sample usage: "curl -X DELETE $HOST:$PORT/order-composite/1".
         *
         * @param orderId Id of the product
         */
        @ResponseStatus(HttpStatus.ACCEPTED)
        @DeleteMapping(value = "/order-composite/{orderId}")
        public Mono<Void> deleteOrder(@PathVariable long orderId);
}
