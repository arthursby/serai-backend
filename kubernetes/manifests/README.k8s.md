migration from helm to skaffold

```
helm template kubernetes/helm/environments/dev-env > kubernetes/manifests/dev-all.yaml 
helm template kubernetes/helm/environments/istio-system > kubernetes/manifests/istio-all.yaml 


eval $(minikube docker-env)
./gradlew build && docker-compose build

kubectl apply -f kubernetes/serai-namespace.yml
kubectl config set-context serai --namespace=serai
kubectl apply -f kubernetes/manifests/dev-all.yaml

kubectl delete -f kubernetes/manifests/dev-all.yaml 
```

