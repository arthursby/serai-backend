### Create kubernete cluster
```
minikube start --memory=10240 --cpus=4 --disk-size=50g --kubernetes-version=1.22.4 --driver=docker 
```
- Install cert-manager
```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
```
- Install Istio + plugins
```
istioctl install --skip-confirmation --set profile=demo --set meshConfig.accessLogFile=/dev/stdout --set meshConfig.accessLogEncoding=JSON

istio_version=$(istioctl version --short --remote=false)
echo "Installing integrations for Istio v$istio_version"
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/${istio_version}/samples/addons/kiali.yaml
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/${istio_version}/samples/addons/jaeger.yaml
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/${istio_version}/samples/addons/prometheus.yaml
kubectl apply -n istio-system -f https://raw.githubusercontent.com/istio/istio/${istio_version}/samples/addons/grafana.yaml

kubectl -n istio-system wait --timeout=600s --for=condition=available deployment --all
helm upgrade --install istio-serai-addons kubernetes/helm/environments/istio-system -n istio-system --wait
```
- Install EFK (Elasticsearch + Fluentd + Kibana)
```
 minikube addons configure efk
```
- Hosts file 
```
127.0.0.1 serai.me kiali.serai.me grafana.serai.me prometheus.serai.me tracing.serai.me kibana.serai.me elasticsearch.serai.me health.serai.me mail.serai.me 
```
- Mail server for testinng
```
kubectl -n istio-system create deployment mail-server --image maildev/maildev:1.1.0
kubectl -n istio-system expose deployment mail-server --port=80,25 --type=ClusterIP
kubectl -n istio-system wait --timeout=60s --for=condition=ready pod -l app=mail-server

kubectl -n istio-system set env deployment/grafana GF_SMTP_ENABLED=true GF_SMTP_SKIP_VERIFY=true GF_SMTP_HOST=mail-server:25 GF_SMTP_FROM_ADDRESS=grafana@serai.me
kubectl -n istio-system wait --timeout=60s --for=condition=ready pod -l app=grafana
```

### Create Namespace with Istio enabled
All the PODs created in the namespace will automatically get istio-proxy containers injected as sidecars.
```
kubectl delete namespace serai
kubectl apply -f kubernetes/serai-namespace.yml
kubectl config set-context serai --namespace=serai
for f in kubernetes/helm/components/*; do helm dep up $f; done
for f in kubernetes/helm/environments/*; do helm dep up $f; done
helm dep ls kubernetes/helm/environments/dev-env/

eval $(minikube  docker-env)
./gradlew build && docker-compose build

helm install serai-dev-env kubernetes/helm/environments/dev-env -n serai --create-namespace --wait
```


### Deploy to Kubernetes

To avoid a slow deployment process due to Kubernetes downloading Docker image, run the following docker pull commands to download the images in advance.
```bash
eval $(minikube --profile=serai docker-env)
docker pull mysql:5.7.32
docker pull rabbitmq:3.8.11-management
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.12.1
docker pull docker.elastic.co/kibana/kibana:7.12.1
```

To see what the manifests will look like, use the helm template command:
```
 helm template kubernetes/helm/environments/dev-env/
```

To verify the cluster will accept the rendered manifest, test with a dry run.
```
helm install --dry-run --debug serai-dev-env kubernetes/helm/environments/dev-env
```

Initiate actual deployment and namespace creation.

```
helm install serai-dev-env kubernetes/helm/environments/dev-env -n serai --create-namespace --wait
helm upgrade serai-dev-env -n serai kubernetes/helm/environments/dev-env --wait
```

### Verify live configuration
```
helm list
kubectl get pods --watch
```

### Prod environment
- Remove rabbitmq and mysql from kubernetes, launch them separately with docker-compose
```
docker-compose  up -d mysql rabbitmq
```
- tag versions
```
docker tag serai/auth-server serai/auth-server:v1
docker tag serai/config-server serai/config-server:v1
docker tag serai/gateway serai/gateway:v1
docker tag serai/order-service serai/order-service:v1
docker tag serai/order-composite-service serai/order-composite-service:v1
```
Prod deployment
```
helm install serai-prod-env kubernetes/helm/environments/prod-env -n serai --create-namespace
```

### Testing istio gateway
For some reason the istio gateway authentication cache may be invalid on startup and give the following error **Jwks doesn't have key to match kid or alg from **

Better enable debug logs with the following
```
istioctl admin log --level authorization:debug
istioctl admin log --level authn:debug

```
It would usually update itself automatically after a few minutes and the requests will work. I haven't found a way to force update it yet.


```
ACCESS_TOKEN=$(curl -k https://writer:secret@serai.me/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
echo ACCESS_TOKEN=$ACCESS_TOKEN
curl -ks https://serai.me/order-composite/ -H "Authorization: Bearer $ACCESS_TOKEN" 
curl -ks https://serai.me/order-composite/ 


kubectl edit RequestAuthentication order-composite-request-authentication
kubectl get RequestAuthentication order-composite-request-authentication -o yaml

POD=$(kubectl get pod -l app=order-composite -n serai -o jsonpath={.items..metadata.name})
kubectl logs $POD
istioctl proxy-config listener ${POD} -n serai -o json 
kubectl logs $(kubectl -n istio-system get pods -l app=istiod -o jsonpath='{.items[0].metadata.name}') -c discovery -n istio-system
kubectl logs $(kubectl get pods -l app=order-composite -o jsonpath='{.items[0].metadata.name}' -n serai) -c istio-proxy -n serai
```

#### Testing; Resilience
- Below tests should be run while running siege:
```
ACCESS_TOKEN=$(curl -k https://writer:secret@serai.me/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
siege https://serai.me/order-composite/ -H "Authorization: Bearer $ACCESS_TOKEN" -c1 -d1 -v
```

- Testing resilience by injecting faults
```
kubectl apply -f kubernetes/resilience-tests/order-virtual-service-with-faults.yml
kubectl delete -f kubernetes/resilience-tests/order-virtual-service-with-faults.yml
```
![Siege Fault Injection](/docs/siege_fault_injection.png)

- Testing resilience by injecting delays
```
kubectl apply -f kubernetes/resilience-tests/order-virtual-service-with-delay.yml
kubectl delete -f kubernetes/resilience-tests/order-virtual-service-with-delay.yml
for i in {1..6}; do time curl -k https://serai.me/order-composite/1 -H "Authorization: Bearer $ACCESS_TOKEN"; done
```
When injecting delays we can see that the requests take an extra 3s to execute.
```
HTTP/1.1 200     0.04 secs:     304 bytes ==> GET  /order-composite/
HTTP/1.1 200     0.05 secs:     304 bytes ==> GET  /order-composite/
HTTP/1.1 200     0.05 secs:     304 bytes ==> GET  /order-composite/
HTTP/1.1 200     3.18 secs:     304 bytes ==> GET  /order-composite/
HTTP/1.1 200     3.03 secs:     304 bytes ==> GET  /order-composite/
HTTP/1.1 200     3.04 secs:     304 bytes ==> GET  /order-composite/
```
Next step is to inject delay to trigger the circuit breaker mechanism.



### Cleamup after use

```
kubectl delete namespace serai
helm uninstall serai-dev-env
helm uninstall serai-prod-env
minikube delete --profile serai
kubectl delete -f kubernetes/efk/fluentd-serai-configmap.yml
kubectl delete -f kubernetes/efk/fluentd-ds.yml
```



### Checking
OC_POD="$(kubectl get pods | grep order-composite | awk '{print $1}')
kubectl describe pod $OC_POD