#!/usr/bin/env bash
#
# Sample usage:
#
#   HOST=localhost PORT=7000 ./test-em-all.bash
#
: ${HOST=serai.me}
: ${PORT=443}
: ${USE_K8S=false}
: ${USE_COMPOSE=false}
: ${HEALTH_URL=https://health.serai.me}
: ${MGM_PORT=4004}
: ${MYSQL_USER=serai}
: ${MYSQL_PASSWORD=demo}
: ${DOCKER_MYSQL=serai-mysql}
: ${NAMESPACE=serai}
: ${SKIP_CB_TESTS=false}
: ${ORDER_ID_EXIST=1}
: ${ORDER_ID_NOT_FOUND=13}
: ${CONFIG_SERVER_USR=serai}
: ${CONFIG_SERVER_PWD=serai}
: ${DEBUG=false}

function assertCurl() {

  local expectedHttpCode=$1
  local curlCmd="$2 -w \"%{http_code}\""
  local result=$(eval $curlCmd)
  local httpCode="${result:(-3)}"
  RESPONSE='' && (( ${#result} > 3 )) && RESPONSE="${result%???}"

  if [ "$httpCode" = "$expectedHttpCode" ]
  then
    if [ "$httpCode" = "200" ]
    then
      echo "Test OK (HTTP Code: $httpCode)"
    else
      echo "Test OK (HTTP Code: $httpCode, $RESPONSE)"
    fi
  else
    echo  "Test FAILED, EXPECTED HTTP Code: $expectedHttpCode, GOT: $httpCode, WILL ABORT!"
    echo  "- Failing command: $curlCmd"
    echo  "- Response Body: $RESPONSE"
    exit 1
  fi
}

function assertEqual() {

  local expected=$1
  local actual=$2

  if [ "$actual" = "$expected" ]
  then
    echo "Test OK (actual value: $actual)"
  else
    echo "Test FAILED, EXPECTED VALUE: $expected, ACTUAL VALUE: $actual, WILL ABORT"
    exit 1
  fi
}

function testUrl() {
  url=$@
  if $url -ks -f -o /dev/null
  then
    return 0
  else
    return 1
  fi;
}

function waitForService() {
  url=$@
  echo -n "Wait for: $url... "
  n=0
  until testUrl $url
  do
    n=$((n + 1))
    if [[ $n == 100 ]]
    then
      echo " Give up"
      exit 1
    else
      sleep 3
      echo -n ", retry #$n "
    fi
  done
  echo "DONE, continues..."
}

# curl -X POST -s -k https://localhost:8443/order-composite -H "Content-Type: application/json" -H "Authorization: Bearer eyJraWQiOiI0ZTY1NTA3MC1lNDNiLTRlOWUtOWRjYy1jOGY4NDZjY2JmZDAiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3cml0ZXIiLCJhdWQiOiJ3cml0ZXIiLCJuYmYiOjE2MzgyNDUwMzYsInNjb3BlIjpbIm9yZGVyOndyaXRlIiwib3BlbmlkIiwib3JkZXI6cmVhZCJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6OTk5OSIsImV4cCI6MTYzODI0ODYzNiwiaWF0IjoxNjM4MjQ1MDM2LCJqdGkiOiJhNTBmZWYwNC0zYWJlLTRhNzctOGVjYi02NzNjYzMzYmYxMjYifQ.iPUth4IycyHUtrqQNGQltkbcj0L1wer18A1_704hQgfS9ao6a0sHMDxYMJxhS_4zt32Nuw0mdNUTdcKx0DFPQJUjqjP4SzVAt54Y312u3OAZ3_S9zKSFa5jVk24FoFjeG52mJto_5sACqTxnmym5Nfs1geKnLF1UEF2obBRyleXSLopM6ibz6X7gBxVfzynsKkyX56d2oq_MHeGcYVzaFuPkkYdgqZ6RuNIzcWo3pZ2vEZhOimdsrVjiCNLYJhm9xtO2tuNDxv_w4rAm2snftGA9rwwUejz6g8sC4AB-OzCeEx7-QDw2YQpFP6KhXUAXDY6EXRuok58lDFgsuZKS-g" --data "{  \"name\": \"**test** - order 1 - all you can eat\",  \"quantity\": 1, \"price\": 100}" -w "%{http_code}"

function cleanupTestData() {
  # Run mysql command to remove test data
  echo "Cleaning up previous order data..."
  #docker exec -it $DOCKER_MYSQL /usr/bin/mysql --user=$MYSQL_USER --password=$MYSQL_PASSWORD --database=order-db --execute "DELETE FROM orders;"
  assertCurl 202 "curl -X DELETE $AUTH -k https://$HOST:$PORT/order-composite/1 -s"
  assertCurl 202 "curl -X DELETE $AUTH -k https://$HOST:$PORT/order-composite/2 -s"
  assertCurl 202 "curl -X DELETE $AUTH -k https://$HOST:$PORT/order-composite/3 -s"
}

function recreateComposite() {
  local composite=$1

  if $DEBUG;  then
    echo "curl -X POST -s -k https://$HOST:$PORT/order-composite -H \"Content-Type: application/json\" -H \"Authorization: Bearer $ACCESS_TOKEN\" --data \"$composite\" "
  fi;
  assertCurl 200 "curl -X POST -s -k https://$HOST:$PORT/order-composite -H \"Content-Type: application/json\" -H \"Authorization: Bearer $ACCESS_TOKEN\" --data \"$composite\" "
}

function recreateCompositeAsync() {
  local composite=$1

  assertCurl 202 "curl -X POST -s -k https://$HOST:$PORT/order-composite/dispatch -H \"Content-Type: application/json\" -H \"Authorization: Bearer $ACCESS_TOKEN\" --data \"$composite\" "
}

function setupTestdata() {
  cleanupTestData

  body='{\"id\":1,\"name\": \"**test** - order 1 - all you can eat\",\"quantity\": 1, \"price\": 100}'
  recreateComposite "$body"

  body='{\"id\":2,\"name\": \"**test** - order 2 - vegetarian\",\"quantity\": 2,\"price\": 200}'
  recreateComposite "$body"

  body='{\"id\":3,\"name\": \"**test** - order 3 - ASYNC -- special mushroom\",\"quantity\": 1,\"price\": 150}'
  recreateCompositeAsync "$body"
}

function testCompositeCreated() {

    # Expect that the Order Composite has been created 
    if ! assertCurl 200 "curl $AUTH -k https://$HOST:$PORT/order-composite/ -s"
    then
        echo -n "FAIL"
        return 1
    fi

    echo "---- BEGIN RESPONSE ----"
    echo $RESPONSE | jq .
    echo "---- END RESPONSE ----"
    set +e
    assertEqual 3 $(echo $RESPONSE | jq .totalItems)

    set -e
}

function waitForMessageProcessing() {
    echo "Wait for messages to be processed... "

    # Give background processing some time to complete...
    sleep 1

    n=0
    until testCompositeCreated
    do
        n=$((n + 1))
        if [[ $n == 40 ]]
        then
            echo " Give up"
            exit 1
        else
            sleep 6
            echo -n ", retry #$n "
        fi
    done
    echo "All messages are now processed!"
}

function testCircuitBreaker() {

    echo "Start Circuit Breaker tests!"

    EXEC="" # default for local usage
    if [[ $USE_K8S == "true" ]]
    then
        EXEC="kubectl -n $NAMESPACE exec deploy/order-composite -c order-composite -- "
    fi

    if [[ $USE_COMPOSE == "true" ]]
    then
        EXEC="docker-compose exec -T order-composite "
    fi

    # First, use the health - endpoint to verify that the circuit breaker is closed
    if [ $DEBUG ] 
    then
      echo "$EXEC curl -s http://localhost:${MGM_PORT}/actuator/health | jq -r .components.circuitBreakers.details.order.details.state)"
    fi
    
    assertEqual "CLOSED" "$($EXEC curl -s http://localhost:${MGM_PORT}/actuator/health | jq -r .components.circuitBreakers.details.order.details.state)"
    # assertEqual "CLOSED" "$(curl -s http://localhost:${MGM_PORT}/actuator/health | jq -r .components.circuitBreakers.details.order.details.state)"

    # Open the circuit breaker by running three slow calls in a row, i.e. that cause a timeout exception
    # Also, verify that we get 500 back and a timeout related error message
    for ((n=0; n<5; n++))
    do
        assertCurl 500 "curl -k https://$HOST:$PORT/order-composite/$ORDER_ID_EXIST?delay=3 $AUTH -s"
        message=$(echo $RESPONSE | jq -r .message)
        assertEqual "Did not observe any item or terminal signal within" "${message:0:50}"
    done

    # Verify that the circuit breaker is open
    assertEqual "OPEN" "$($EXEC curl -s http://localhost:${MGM_PORT}/actuator/health | jq -r .components.circuitBreakers.details.order.details.state)"

    # Verify that the circuit breaker now is open by running the slow call again, verify it gets 200 back, i.e. fail fast works, and a response from the fallback method.
    assertCurl 200 "curl -k https://$HOST:$PORT/order-composite/$ORDER_ID_EXIST?delay=3 $AUTH -s"
    assertEqual "fallback order$ORDER_ID_EXIST" "$(echo "$RESPONSE" | jq -r .name)"

    # Also, verify that the circuit breaker is open by running a normal call, verify it also gets 200 back and a response from the fallback method.
    assertCurl 200 "curl -k https://$HOST:$PORT/order-composite/$ORDER_ID_EXIST $AUTH -s"
    assertEqual "fallback order$ORDER_ID_EXIST" "$(echo "$RESPONSE" | jq -r .name)"

    # # Verify that a 404 (Not Found) error is returned for a non existing orderId ($ORDER_ID_NOT_FOUND) from the fallback method.
    # assertCurl 404 "curl -k https://$HOST:$PORT/order-composite/$ORDER_ID_NOT_FOUND $AUTH -s"
    # assertEqual "Order Id: $ORDER_ID_NOT_FOUND not found in fallback cache!" "$(echo $RESPONSE | jq -r .message)"

    # Wait for the circuit breaker to transition to the half open state (i.e. max 10 sec)
    echo "Will sleep for 10 sec waiting for the CB to go Half Open..."
    sleep 10

    # Verify that the circuit breaker is in half open state
    assertEqual "HALF_OPEN" "$($EXEC curl -s http://localhost:${MGM_PORT}/actuator/health | jq -r .components.circuitBreakers.details.order.details.state)"

    # Close the circuit breaker by running three normal calls in a row
    for ((n=0; n<3; n++))
    do
        assertCurl 200 "curl -k https://$HOST:$PORT/order-composite/$ORDER_ID_EXIST $AUTH -s"
        assertEqual "$ORDER_ID_EXIST" "$(echo "$RESPONSE" | jq -r .id)"
    done

    # Verify that the circuit breaker is in closed state again
    assertEqual "CLOSED" "$($EXEC curl -s http://localhost:${MGM_PORT}/actuator/health | jq -r .components.circuitBreakers.details.order.details.state)"

    # Verify that the expected state transitions happened in the circuit breaker
    assertEqual "CLOSED_TO_OPEN"      "$($EXEC curl -s http://localhost:${MGM_PORT}/actuator/circuitbreakerevents/order/STATE_TRANSITION | jq -r .circuitBreakerEvents[-3].stateTransition)"
    assertEqual "OPEN_TO_HALF_OPEN"   "$($EXEC curl -s http://localhost:${MGM_PORT}/actuator/circuitbreakerevents/order/STATE_TRANSITION | jq -r .circuitBreakerEvents[-2].stateTransition)"
    assertEqual "HALF_OPEN_TO_CLOSED" "$($EXEC curl -s http://localhost:${MGM_PORT}/actuator/circuitbreakerevents/order/STATE_TRANSITION | jq -r .circuitBreakerEvents[-1].stateTransition)"
}

set -e

echo "Start Tests:" `date`

echo "HOST=${HOST}"
echo "PORT=${PORT}"
echo "USE_K8S=${USE_K8S}"
echo "USE_COMPOSE=${USE_COMPOSE}"
echo "HEALTH_URL=${HEALTH_URL}"
echo "MGM_PORT=${MGM_PORT}"
echo "SKIP_CB_TESTS=${SKIP_CB_TESTS}"

if [[ $@ == *"start"* ]]
then
  echo "Restarting the test environment..."
  echo "$ docker-compose down --remove-orphans"
  docker-compose down --remove-orphans
  echo "$ docker-compose up -d"
  docker-compose up -d
fi

waitForService curl -k $HEALTH_URL/actuator/health

ACCESS_TOKEN=$(curl -k https://writer:secret@$HOST:$PORT/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
if $DEBUG;  then
  echo "curl -k https://serai:serai@$HOST:$PORT/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r"
  echo "ACCESS_TOKEN=$ACCESS_TOKEN"
fi
AUTH="-H \"Authorization: Bearer $ACCESS_TOKEN\""

setupTestdata

waitForMessageProcessing

# Verify that the reader - client with only read scope can call the read API but not delete API.
READER_ACCESS_TOKEN=$(curl -k https://reader:secret@$HOST:$PORT/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
# echo READER_ACCESS_TOKEN=$READER_ACCESS_TOKEN
READER_AUTH="-H \"Authorization: Bearer $READER_ACCESS_TOKEN\""

# Verify access to Swagger and OpenAPI URLs
echo "Swagger/OpenAPI tests"
assertCurl 302 "curl -ks  https://$HOST:$PORT/openapi/swagger-ui.html"
assertCurl 200 "curl -ksL https://$HOST:$PORT/openapi/swagger-ui.html"
assertCurl 200 "curl -ks  https://$HOST:$PORT/v3/api-docs"
assertEqual "3.0.1" "$(echo $RESPONSE | jq -r .openapi)"
# assertEqual "https://$HOST:$PORT" "$(echo $RESPONSE | jq -r .servers[].url)"
assertCurl 200 "curl -ks  https://$HOST:$PORT/v3/api-docs.yaml"

if [[ $SKIP_CB_TESTS == "false" ]]
then
    testCircuitBreaker
fi

if [[ $@ == *"stop"* ]]
then
    echo "We are done, stopping the test environment..."
    echo "$ docker-compose down"
    docker-compose down
fi

echo "End, all tests OK:" `date`
