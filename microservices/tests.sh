#!/bin/bash

echo "Don't run it directly. Make to be used as scratchbook"
exit 1

curl $HOST:$PORT/order/ | jq .
curl -X POST http://localhost:8001/order -H "Content-Type: application/json" --data "{\"orderId\":1, \"name\": \"**test** - order 1 - all you can eat\",\"quantity\": 1, \"price\": 100}"

ACCESS_TOKEN=$(curl -k http://writer:secret@localhost:9999/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
echo $ACCESS_TOKEN
# # List existing orders
# curl -s http://localhost:8000/order-composite/ | jq .

# Order creation
curl -X POST http://localhost:8001/order -H "Content-Type: application/json" --data "{\"id\":1,\"name\": \"**test** - order 1 - all you can eat\",\"quantity\": 1, \"price\": 100}"
curl -X POST http://localhost:8001/order -H "Content-Type: application/json" --data "{\"name\": \"**test** - order 1 - all you can eat\",\"quantity\": 1, \"price\": 100}"

# # Normal calls
curl -s -i http://localhost:8000/order-composite/1
curl -s -i http://localhost:8001/order/1
curl -s -i https://localhost:8445/order-composite/1
curl -ksi https://serai.me/order-composite/11

## Delete Calls
curl -i -s -X DELETE http://localhost:8001/order/1
curl -i -s -X DELETE http://localhost:8000/order-composite/1 
curl -i -s -X DELETE http://localhost:8000/order-composite/1 -H "Authorization: Bearer $ACCESS_TOKEN"  
curl -i -s -k -X DELETE https://localhost:8445/order-composite/1 -H "Authorization: Bearer $ACCESS_TOKEN"  

ACCESS_TOKEN=$(curl -k https://writer:secret@serai.me/oauth2/token -d grant_type=client_credentials -s | jq .access_token -r)
for ((n=10; n<11; n++))
do
    data="{\"id\":$n,\"name\": \"**test** - order $n\",\"quantity\": 1, \"price\": 100}"
    curl -ksi https://serai.me/order-composite/ -H "Authorization: Bearer $ACCESS_TOKEN" -H "Content-Type: application/json" --data "$data"
    sleep 1
done

