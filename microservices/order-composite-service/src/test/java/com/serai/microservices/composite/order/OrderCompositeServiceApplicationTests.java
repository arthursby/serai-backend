package com.serai.microservices.composite.order;


import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(
  webEnvironment = WebEnvironment.RANDOM_PORT,
  classes = {TestSecurityConfig.class},
  properties = {
    "spring.security.oauth2.resourceserver.jwt.issuer-uri=",
    "spring.main.allow-bean-definition-overriding=true",
    "spring.cloud.config.enabled=false"})
class OrderCompositeServiceApplicationTests {
  

}
