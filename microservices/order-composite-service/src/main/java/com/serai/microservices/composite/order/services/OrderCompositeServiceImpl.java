package com.serai.microservices.composite.order.services;

import static java.util.logging.Level.FINE;

import java.net.URL;
import java.util.List;

import com.serai.api.composite.order.OrderCompositeService;
import com.serai.api.core.order.OrderDTO;
import com.serai.api.exceptions.InvalidInputException;
import com.serai.api.exceptions.NotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import reactor.core.publisher.Mono;

@RestController
public class OrderCompositeServiceImpl implements OrderCompositeService {

  private static final Logger LOG = LoggerFactory.getLogger(OrderCompositeServiceImpl.class);

  private final RabbitTemplate rabbitTemplate;
  private final String orderService;
  private final Integer orderServicePort;
  private final String orderServiceHealthURL;
  private final String ordersTopicExchange;
  private final String routineKey;
  private final ModelMapper mapper;
  private final WebClient webClient;

  private final RestTemplate restTemplate;

  @Autowired
  public OrderCompositeServiceImpl(WebClient.Builder webClient,
      RestTemplate restTemplate,
      RabbitTemplate rabbitTemplate,
      ModelMapper mapper,
      @Value("${app.order-service}") String orderService,
      @Value("${app.order-service-port}") Integer orderServicePort,
      @Value("${app.order-service-health-url}") String orderServiceHealthURL,
      @Value("${amqp.topic}") String ordersTopicExchange,
      @Value("${amqp.routine_key}") String routineKey) {

    this.webClient = webClient.build();
    this.orderService = orderService;
    this.orderServicePort = orderServicePort;
    this.orderServiceHealthURL = orderServiceHealthURL;
    this.rabbitTemplate = rabbitTemplate;
    this.mapper = mapper;
    this.routineKey = routineKey;
    this.ordersTopicExchange = ordersTopicExchange;
    this.restTemplate = restTemplate;
  }

  @Override
  public Mono<String> find(String name, int page, int size, String[] sort) {
    try {
      String url = "http://" + orderService + ":" + orderServicePort + "/order";
      LOG.debug("Will call getOrder API on URL: {}", url);
      LOG.debug("params are name={} page={}, size={}, sort={}", name, page, size, sort);
      String rawData = restTemplate.getForObject(url, String.class, name, page, size, sort);

      LOG.debug("raw", rawData);
      return Mono.just(rawData);
    } catch (HttpClientErrorException ex) {
      switch (ex.getStatusCode()) {
        case NOT_FOUND:
          throw new NotFoundException();

        case UNPROCESSABLE_ENTITY:
          throw new InvalidInputException();

        default:
          LOG.warn("Got an unexpected HTTP error: {}, will rethrow it", ex.getStatusCode());
          LOG.warn("Error body: {}", ex.getResponseBodyAsString());
          throw ex;
      }
    }
  }

  @Override
  public Mono<ResponseEntity<OrderDTO>> createOrder(OrderDTO body) {
    try {
      String url = "http://" + orderService + ":" + orderServicePort + "/order";
      LOG.debug("Will call orderService API on URL: ", url);

      OrderDTO newOrder = restTemplate.postForObject(url, body, OrderDTO.class);
      LOG.debug("Created order with id: {}", newOrder.getName());

      return Mono.just(ResponseEntity.ok(newOrder));

    } catch (HttpClientErrorException ex) {

      switch (ex.getStatusCode()) {
        case NOT_FOUND:
          throw new NotFoundException();

        case UNPROCESSABLE_ENTITY:
          throw new InvalidInputException();

        default:
          LOG.warn("Got an unexpected HTTP error: {}, will rethrow it", ex.getStatusCode());
          LOG.warn("Error body: {}", ex.getResponseBodyAsString());
          throw ex;
      }
    }
  }

  @Override
  public Mono<ResponseEntity<HttpStatus>> dispatchOrder(OrderDTO body) {
    LOG.debug("Sennding to  with routineKey '{}' on topic {}", routineKey, ordersTopicExchange);

    try {
      rabbitTemplate.convertAndSend(ordersTopicExchange, routineKey, body);
    } catch (HttpClientErrorException ex) {

      switch (ex.getStatusCode()) {
        case NOT_FOUND:
          throw new NotFoundException();

        case UNPROCESSABLE_ENTITY:
          throw new InvalidInputException();

        default:
          LOG.warn("Got an unexpected HTTP error: {}, will rethrow it", ex.getStatusCode());
          LOG.warn("Error body: {}", ex.getResponseBodyAsString());
          throw ex;
      }
    }

    return Mono.just(new ResponseEntity<>(HttpStatus.ACCEPTED));
  }

  @Override
  public ResponseEntity<HttpStatus> secured(String host) {
    LOG.info("Secure test with {}", host);

    String url = orderService;
    if (host != null) {
      url = host;
    }
    String param = "test";
    String response = restTemplate.getForObject(url, String.class, param);

    LOG.debug("secured response", response);

    return new ResponseEntity<>(HttpStatus.OK);
  }

  @Override
  public Mono<String> status() {
    SecurityContext ctx = SecurityContextHolder.getContext();
    LOG.debug("status ctx", ctx);
    return Mono.just("OK");
  }

  public Mono<Health> getOrderHealth() {
    return getHealth(orderServiceHealthURL);
  }

  private Mono<Health> getHealth(String url) {
    url += "/actuator/health";
    LOG.debug("Will call the Health API on URL: {}", url);
    return webClient.get().uri(url).retrieve().bodyToMono(String.class)
        .map(s -> new Health.Builder().up().build())
        .onErrorResume(ex -> Mono.just(new Health.Builder().down(ex).build()))
        .log(LOG.getName(), FINE);
  }

  @Override
  @Retry(name = "order")
  @TimeLimiter(name = "order")
  @CircuitBreaker(name = "order", fallbackMethod = "getOrderFallback")
  public Mono<OrderDTO> getOrder(HttpHeaders headers, long orderId, int delay, int faultPercent) {
    LOG.info("Will get composite order info for order.id={}", orderId);
    String url = "http://" + orderService + ":" + orderServicePort + "/order/" + orderId + "?delay=" + delay + "&faultPercent=" + faultPercent;
    LOG.debug("Will call getOrder API on URL: {}", url);

    return webClient.get().uri(url)
      .headers(h -> h.addAll(headers))
      .retrieve().bodyToMono(OrderDTO.class).log(LOG.getName(), FINE);
  }

  private Mono<OrderDTO> getOrderFallback(HttpHeaders headers, long orderId, int delay, int faultPercent,
  CallNotPermittedException ex) {

    LOG.warn("Creating a fail-fast fallback order for orderId = {}, delay = {}, faultPercent = {} and exception = {} ",
        orderId, delay, faultPercent, ex.toString());

    if (orderId < 1) {
      throw new InvalidInputException("Invalid orderId: " + orderId);
    }

    if (orderId == 13) {
      String errMsg = "Order Id: " + orderId + " not found in fallback cache!";
      LOG.warn(errMsg);
      throw new NotFoundException(errMsg);
    }

    OrderDTO fallback = new OrderDTO();
    fallback.setId(orderId);
    fallback.setName("fallback order"+orderId);
    fallback.setPrice(100);
    fallback.setQuantity(1);
    return Mono.just(fallback);
  }

  @Override
  public Mono<Void> deleteOrder(long orderId) {
    LOG.info("delete order {}", orderId);
    SecurityContext sc = SecurityContextHolder.getContext();
    logAuthorizationInfo(sc);

    String url = "http://" + orderService + ":" + orderServicePort + "/order/" + orderId;
    restTemplate.delete(url);
    return null;
  }

  private void logAuthorizationInfo(SecurityContext sc) {
    if (sc != null && sc.getAuthentication() != null && sc.getAuthentication() instanceof JwtAuthenticationToken) {
      Jwt jwtToken = ((JwtAuthenticationToken) sc.getAuthentication()).getToken();
      logAuthorizationInfo(jwtToken);
    } else {
      LOG.warn("No JWT based Authentication supplied, running tests are we?");
    }
  }

  private void logAuthorizationInfo(Jwt jwt) {
    if (jwt == null) {
      LOG.warn("No JWT supplied, running tests are we?");
    } else {
      if (LOG.isDebugEnabled()) {
        URL issuer = jwt.getIssuer();
        List<String> audience = jwt.getAudience();
        Object subject = jwt.getClaims().get("sub");
        Object scopes = jwt.getClaims().get("scope");
        Object expires = jwt.getClaims().get("exp");

        LOG.debug("Authorization info: Subject: {}, scopes: {}, expires {}: issuer: {}, audience: {}", subject, scopes,
            expires, issuer, audience);
      }
    }
  }

}
