
package com.serai.microservices.composite.order;

import static org.springframework.http.HttpMethod.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity(debug=false)
@EnableAutoConfiguration
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  private static final Logger LOG = LoggerFactory.getLogger(SecurityConfig.class);

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    LOG.debug("Order composite security filter enabled");

    http.csrf().disable().authorizeRequests()
      .mvcMatchers("/openapi/**").permitAll()
      .mvcMatchers("/webjars/**").permitAll()
      .mvcMatchers("/actuator/**").permitAll()
      .mvcMatchers("/swagger-ui.html").permitAll()
      .mvcMatchers("/v3/**").permitAll()
      // .mvcMatchers("/order-composite/status").hasAuthority("SCOPE_order:read")
      // .mvcMatchers("/order-composite/secured").hasRole("ROLE_ADMIN")
      // .mvcMatchers(POST, "/order-composite/").hasAuthority("SCOPE_order:write")
      // .mvcMatchers(POST, "/order-composite/dispatch").hasAuthority("SCOPE_order:write")
      .mvcMatchers(GET, "/order-composite/**").permitAll()
      .anyRequest().authenticated()
      .and()
      .oauth2ResourceServer()
      .jwt();
  }
}