package com.serai.microservices.composite.order;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;


@SpringBootApplication
@ComponentScan("com.serai")
@EnableRabbit
public class OrderCompositeServiceApplication {

	private static final Logger LOG = LoggerFactory.getLogger(OrderCompositeServiceApplication.class);

	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public WebClient.Builder loadBalancedWebClientBuilder() {
	  return WebClient.builder();
	}

	public static void main(String[] args) {
		LOG.info("Initializing order composite service api...");
		SpringApplication.run(OrderCompositeServiceApplication.class, args);
	}

}
