package com.serai.microservices.composite.order.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configures RabbitMQ via AMQP abstraction to use events in our application.
 */
@Configuration
public class AMQPConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(AMQPConfiguration.class);

    @Bean
    public TopicExchange ordersTopicExchange(
            @Value("${amqp.topic}") final String topicName) {
                LOG.info("Setting topic exchange '{}'", topicName);
        return ExchangeBuilder.topicExchange(topicName).durable(true).build();
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
