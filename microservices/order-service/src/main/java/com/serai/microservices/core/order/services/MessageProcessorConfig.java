package com.serai.microservices.core.order.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.serai.api.core.order.OrderDTO;
import com.serai.api.core.order.OrderService;


@Configuration
public class MessageProcessorConfig {

  private static final Logger LOG = LoggerFactory.getLogger(MessageProcessorConfig.class);

  private final OrderService orderService;

  @Autowired
  public MessageProcessorConfig(OrderService orderService) {
    this.orderService = orderService;
  }

  @RabbitListener(queues = {"${amqp.queue}"})
  public void receive(OrderDTO order) {
    LOG.debug("Order dispatch message received for order ", order.getName());
      try {
        orderService.createOrder(order);
        LOG.info("Order has been saved successfuly.");
      } catch(Exception e) {
        LOG.error("No more time to finish the project...");
        throw e;
      }
  }
}
