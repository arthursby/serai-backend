package com.serai.microservices.core.order;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.serai")
@EnableRabbit
public class OrderServiceApplication {

    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceApplication.class);

	public static void main(String[] args) {
        LOG.info("Initializing order service api...");
		SpringApplication.run(OrderServiceApplication.class, args);
	}

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
