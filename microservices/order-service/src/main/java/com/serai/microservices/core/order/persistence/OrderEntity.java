package com.serai.microservices.core.order.persistence;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    private long id;

    private String name;
    private int quantity;
    private int price;

    @Version
    private int version;

    public OrderEntity() {
    }

    public OrderEntity(int id, String name, int quantity, int price, int version) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.version = version;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

}
