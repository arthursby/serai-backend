package com.serai.microservices.core.order.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.messaging.handler.annotation.support.MessageHandlerMethodFactory;

@Configuration
public class AMQPConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(AMQPConfiguration.class);

        @Value("${amqp.routine_key}")
	String routineKey;

    @Bean
    public TopicExchange ordersTopicExchange(
            @Value("${amqp.topic}") final String topicExchangeName) {
                LOG.info("Setting topic '{}'", topicExchangeName);
        return ExchangeBuilder.topicExchange(topicExchangeName).durable(true).build();
    }

    @Bean
    public Queue ordersQueue(
            @Value("${amqp.queue}") final String queueName) {
                LOG.info("Setting queue '{}'", queueName);
        return QueueBuilder.durable(queueName).build();
    }

    
    @Bean
    public Binding correctAttemptsBinding(final Queue ordersQueue,
                                          final TopicExchange topicExchange) {
                LOG.info("Binding topic '{}' on queue '{}' with routinekey '{}'", topicExchange.getName(), ordersQueue.getActualName(), routineKey);
        return BindingBuilder.bind(ordersQueue)
                .to(topicExchange)
                .with(routineKey);
    }

    @Bean
    public MessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();

        final MappingJackson2MessageConverter jsonConverter =
                new MappingJackson2MessageConverter();
        jsonConverter.getObjectMapper().registerModule(
                new ParameterNamesModule(JsonCreator.Mode.PROPERTIES));

        factory.setMessageConverter(jsonConverter);
        return factory;
    }

    @Bean
    public RabbitListenerConfigurer rabbitListenerConfigurer(
            final MessageHandlerMethodFactory messageHandlerMethodFactory) {
        return (c) -> c.setMessageHandlerMethodFactory(messageHandlerMethodFactory);
    }

}
