package com.serai.microservices.core.order.persistence;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity, Long>{
    Page<OrderEntity> findByNameContaining(String title, Pageable pageable);
}
