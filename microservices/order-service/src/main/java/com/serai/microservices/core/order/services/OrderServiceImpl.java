package com.serai.microservices.core.order.services;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;

import com.serai.api.core.order.OrderDTO;
import com.serai.api.core.order.OrderService;
import com.serai.api.exceptions.InvalidInputException;
import com.serai.api.exceptions.NotFoundException;
import com.serai.api.util.PageResult;
import com.serai.microservices.core.order.persistence.OrderEntity;
import com.serai.microservices.core.order.persistence.OrderRepository;

@RestController
public class OrderServiceImpl implements OrderService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private final OrderRepository repository;

    @Autowired
    private final ModelMapper mapper;

    @Autowired
    public OrderServiceImpl(OrderRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }

        return Sort.Direction.ASC;
    }

    public ResponseEntity<PageResult<OrderDTO>> find(String name, int page, int size, String[] sort) {
        try {
            List<Sort.Order> sqlOrders = new ArrayList<>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    sqlOrders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                sqlOrders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<OrderEntity> pizzaOrdersEntities;
            List<OrderDTO> pizzaOrdersDTOs = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size, Sort.by(sqlOrders));

            Page<OrderEntity> pageOrders;
            if (name == null)
                pageOrders = repository.findAll(paging);
            else
                pageOrders = repository.findByNameContaining(name, paging);

            pizzaOrdersEntities = pageOrders.getContent();
            // convert entity to DTO
            for (int i = 0; i < pizzaOrdersEntities.size(); i++) {
                OrderEntity poe = pizzaOrdersEntities.get(i);
                OrderDTO pot = new OrderDTO();
                mapper.map(poe, pot);

                pizzaOrdersDTOs.add(pot);
            }

            PageResult<OrderDTO> response = new PageResult<>();
            response.setResults(pizzaOrdersDTOs);
            response.setCurrentPage(pageOrders.getNumber());
            response.setTotalItems(pageOrders.getTotalElements());
            response.setTotalPages(pageOrders.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public OrderDTO createOrder(OrderDTO body) {
        try {
            OrderEntity entity = mapper.map(body, OrderEntity.class);
            OrderEntity newEntity = repository.save(entity);

            LOG.debug("createOrder: entity created for order: {}", body.getName());
            return mapper.map(newEntity, OrderDTO.class);

        } catch (DuplicateKeyException dke) {
            throw new InvalidInputException("Duplicate key, Order: " + body.getName());
        }
    }

    @Override
    public OrderDTO getOrder(long orderId, int delay, int faultPercent) {
        if (orderId < 1) {
            throw new InvalidInputException("Invalid orderId: " + orderId);
        }

        LOG.info("Will get order info for id={}", orderId);

        Optional<OrderEntity> ooe = repository.findById(orderId);
        if(!ooe.isPresent()) {
            throw new NotFoundException("invalid orderId: " + orderId);
        }
        
        OrderEntity entity = ooe.get();
        throwErrorIfBadLuck(entity, faultPercent);

        if(delay>0) {
            LOG.info("Simulate method delay {} seconds", delay);
            try {
                TimeUnit.SECONDS.sleep(delay);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        OrderDTO result = mapper.map(entity, OrderDTO.class);
        return result;
    }

    @Override
    public ResponseEntity<HttpStatus> deleteOrder(long orderId) {

        if (orderId < 1) {
            throw new InvalidInputException("Invalid orderId: " + orderId);
        }

        LOG.debug("deleteOrder: tries to delete an entity with orderId: {}", orderId);
        // Only delete products that are found
        Optional<OrderEntity> entity = repository.findById(orderId);
        if(!entity.isPresent()) {
            LOG.warn("Order id={} not found - can't delete", orderId);
        } else {
            repository.delete(entity.get());
            LOG.debug("Order id={} has been deleted", orderId);
        }

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    private OrderEntity throwErrorIfBadLuck(OrderEntity entity, int faultPercent) {

        if (faultPercent == 0) {
            return entity;
        }

        int randomThreshold = getRandomNumber(1, 100);

        if (faultPercent < randomThreshold) {
            LOG.debug("We got lucky, no error occurred, {} < {}", faultPercent, randomThreshold);
        } else {
            LOG.debug("Bad luck, an error occurred, {} >= {}", faultPercent, randomThreshold);
            throw new RuntimeException("Something went wrong...");
        }

        return entity;
    }

    private final Random randomNumberGenerator = new Random();

    private int getRandomNumber(int min, int max) {

        if (max < min) {
            throw new IllegalArgumentException("Max must be greater than min");
        }

        return randomNumberGenerator.nextInt((max - min) + 1) + min;
    }

}
