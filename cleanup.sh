#!/bin/bash
# Utility script to re-create instances from clean images

docker-compose down --remove-orphans
./gradlew clean
docker rm $(docker container ls -aq -f name=serai)
docker rmi $(docker images serai/* -q)


# ./gradlew build
# docker-compose build --no-cache